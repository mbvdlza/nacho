from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    # admin/ stuff
    url(r'^admin/', include(admin.site.urls)),

    # django-registration
    url(r'^accounts/', include('registration.urls')),

    # newsletter urls
    url(r'^newsletter/', include('newsletter.urls')),


    # subscription urls
    url(r'^subscription/', include('subscription.urls')),


    # map / to newsletter/ index
    url(r'^', include('newsletter.urls', namespace="newsletter"))

)
