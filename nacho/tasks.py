from celery.decorators import periodic_task
from mailer.engine import send_all

from datetime import timedelta

@periodic_task(run_every=timedelta(seconds=20))
def email_tasks():
    send_all()
