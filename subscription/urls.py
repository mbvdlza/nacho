from django.conf.urls import url, include
from subscription import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # fooling around, capturing bound and unbound.
    url(r'^subscribe/([0-9])/([0-9])/$', views.subscribe, name='newsletter_subscribe'),
    url(r'^unsubscribe/(?P<newsletter_id>[0-9])/(?P<email_id>[0-9])/$', views.unsubscribe, name='newsletter_unsubscribe'),

]
