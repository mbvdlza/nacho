from django.shortcuts import render
from subscription.models import Subscription
from newsletter.models import Newsletter, Email
from newsletter.contextkit import contemplate_newsletterDetail, get_subscriptions
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.utils import simplejson

def index(request):
    pass 

def subscribe(request, newsletter_id, email_id):
    nl = Newsletter.objects.get(id=newsletter_id)
    user_id = request.user.id
    emailadds = Email.objects.filter(user_id=request.user.id)

    subbed = Subscription.objects.filter(newsletter=newsletter_id, user=user_id, email=email_id)
    if not subbed:
        subscription = Subscription(newsletter=newsletter_id, \
            user=user_id, email=email_id)
        subscription.save()

    subscripts = get_subscriptions(newsletter_id, request.user.id)
    renderedContext = contemplate_newsletterDetail(request, \
        emailadds, subscripts, nl, user_id)

    return HttpResponse(renderedContext)


def unsubscribe(request, newsletter_id, email_id):
    nl = Newsletter.objects.get(id=newsletter_id)
    user = request.user
    email = Email.objects.get(id=email_id)

    emailadds = Email.objects.filter(user_id=request.user.id)
    subbed = Subscription.objects.filter(newsletter=nl.id, user=user.id, email=email.id)
    if subbed:
        subbed.delete()

    subscripts = get_subscriptions(newsletter_id, request.user.id)

    context = RequestContext(request, {
        'emailadds': emailadds,
        'subscripts': subscripts,
        'nl': nl,
        'userid': request.user.id,
    })

    template = loader.get_template('newsletter/detail.html')

    return HttpResponse(template.render(context))



