from django.db import models

class Subscription(models.Model):
    newsletter = models.IntegerField()
    user = models.IntegerField()
    email = models.IntegerField()

    def __unicode__(self):
        return "newsletter=%d,user=%d,email=%d" \
            % (self.newsletter, self.user, self.email)

