class SubscriptionRouter(object):
    """
    DB routing all queries on subscriptions app to the 'secondary' database.
    """
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'subscription':
            return 'secondary'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'subscription':
            return 'secondary'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'subscription' or \
           obj2._meta.app_label == 'subscription':
           return True
        return None

    def allow_syncdb(self, db, model):
        if db == 'secondary':
            return model._meta.app_label == 'subscription'
        elif model._meta.app_label == 'subscription':
            return False
        return None

