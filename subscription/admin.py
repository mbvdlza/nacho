from django.contrib import admin
from subscription.models import Subscription

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'newsletter']
    list_filter = ['email', 'user', 'newsletter']

admin.site.register(Subscription, SubscriptionAdmin)

