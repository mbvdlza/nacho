from django.template import RequestContext, loader
from django.shortcuts import render
from subscription.models import Subscription
from newsletter.models import Email

def contemplate_newsletterDetail(request, emailadds, subscripts, newsletter,
uid):

    context = RequestContext(request, {
        'emailadds': emailadds,
        'subscripts': subscripts,
        'nl': newsletter,
        'userid': uid,
    })

    template = loader.get_template('newsletter/detail.html')
    return template.render(context)

def get_subscriptions(newsletter_id, user_id):
    subsResult = Subscription.objects.filter(newsletter=newsletter_id, user=user_id)

    allsubs = []
    for sub in subsResult:
        tmpsub = {}
        tmpmail = Email.objects.get(id=sub.email)
        if tmpmail:
            tmpsub['email'] = tmpmail.email
            tmpsub['emailid'] = tmpmail.id
        allsubs.append(tmpsub)
    return allsubs

