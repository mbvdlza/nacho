from django.contrib import admin
from newsletter.models import Newsletter, Email

class NewsletterAdmin(admin.ModelAdmin):
    fieldsets = [
        ('MetaInfo', { 'fields': ['content', 'title']}),
        ('Status', { 'fields': ['active']}),
    ]

    list_display = ['active', 'content', 'title']
    list_filter = ['active']


class EmailAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'active')
    list_filter = ['active']

admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Email, EmailAdmin)

