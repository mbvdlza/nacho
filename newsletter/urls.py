from django.conf.urls import url, include
from newsletter import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'^newsletter/([0-9]+)/$', views.newsletterDetail, name='newsletterDetail'),
    url(r'^(?P<newsletter_id>[0-9]+)/$', views.newsletterDetail, name='newsletterDetail'),
    #url(r'^ajax_subscribe/([0-9]+)/([0-9]+)/$', views.ajax_subscribe, name='ajax_subscribe'),
    #url(r'^ajax_unsubscribe/([0-9]+)/([0-9]+)/$', views.ajax_unsubscribe, name='ajax_unsubscribe'),
    url(r'^managemails/$', views.managemails, name='managemails'),
    url(r'^managemails/(?P<email_id>[0-9]+)/$', views.managemails, name='deletemail'),
    url(r'^maildispatch/$', views.maildispatch, name='maildispatch'),

    #url(r'^subscribe/([0-9])/([0-9])/$', views.subscribe, name='newsletter_subscribe'),
    #url(r'^unsubscribe/([0-9])/([0-9])/$', views.unsubscribe, name='newsletter_unsubscribe'),

]
