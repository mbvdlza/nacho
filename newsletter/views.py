from django.shortcuts import render, render_to_response
from newsletter.models import Newsletter, Email, EmailForm
from subscription.models import Subscription
from django.template import RequestContext, loader
from django.http import HttpResponse
from newsletter.contextkit import contemplate_newsletterDetail, get_subscriptions
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from nacho.tasks import email_tasks

def index(request):
    newslettersAvail = Newsletter.objects.all()
    newslettersCount = Newsletter.objects.count()
    context = RequestContext(request, {
        'sendables': newslettersAvail,
        'count': newslettersCount,
        'accstat': request.user.is_active,
    })

    if not request.user.is_authenticated():
        template = loader.get_template('newsletter/noauth.html')
    else:
        template = loader.get_template('newsletter/index.html')

    return HttpResponse(template.render(context))

def newsletterDetail(request, newsletter_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/newsletter/")

    emailadds = Email.objects.filter(user_id=request.user.id)
    newsletter = Newsletter.objects.get(id=newsletter_id)
    allsubs = get_subscriptions(newsletter_id, request.user.id)
    context = RequestContext(request, {
        'emailadds': emailadds,
        'subscripts': allsubs,
        'nl': newsletter,
        'userid': request.user.id,
        'accstat': request.user.is_active,
    })

    template = loader.get_template('newsletter/detail.html')

    #if request.is_ajax():
    #    raise Exception("Is AJAX.")

    return HttpResponse(template.render(context))

def managemails(request, email_id=False):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/newsletter/")

    if email_id:
        Email.objects.filter(id=email_id).delete()
        # if a subscription exists for a mail being deleted, delete the subscription too
        sub = Subscription.objects.filter(user=request.user.id, email=email_id)
        if sub:
             Subscription.objects.filter(user=request.user.id, email=email_id).delete()

    problem = ""
    if request.method == 'POST':
        form = EmailForm(request.POST)
        try:
            mail = form.save(commit=False)
        except ValidationError as ve:
            problem = "Duplicate Email"

        # this will become False, when email activation is implemented.
        mail.active = request.user.is_active;
        mail.user = request.user;
        if form.is_valid():
            form.save()
        else:
            problem = "Invalid"
    else:
        form = EmailForm()

    emailadds = Email.objects.filter(user_id=request.user.id)

    variables = RequestContext(request, {
        'form': form,
        'problem': problem,
        'emailadds': emailadds,
        'accstat': request.user.is_active,
    })
    return render_to_response('newsletter/managemails.html', variables)

def maildispatch(request):

    subscripts = Subscription.objects.all()

    for sub in subscripts:
        newsletter = Newsletter.objects.get(id=sub.newsletter)
        mail = Email.objects.get(id=sub.email)
        subject = newsletter.title
        recipient = mail.email
        content = newsletter.content

        send_mail(subject, content, "marlon@kbye.co.za", [recipient], fail_silently=False)

    newslettersAvail = Newsletter.objects.all()
    newslettersCount = Newsletter.objects.count()
    context = RequestContext(request, {
        'sendables': newslettersAvail,
        'count': newslettersCount,
        'accstat': request.user.is_active,
    })

    if not request.user.is_authenticated():
        template = loader.get_template('newsletter/noauth.html')
    else:
        template = loader.get_template('newsletter/index.html')

    return HttpResponse(template.render(context))

