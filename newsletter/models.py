from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from django.forms import ModelForm

class Email(models.Model):
    user = models.ForeignKey(User, related_name="emailuser")
    email = models.EmailField()
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.email

class Newsletter(models.Model):
    content = models.CharField(max_length=255)
    title = models.CharField(max_length=64)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "title=%s,content=%s,active=%s" % \
            (self.title, self.content, self.title)

def update_emails(sender, instance, **kwargs):
    """
    Post_save signal from User, to populate our Email model.
    """
    if instance.email:
        if Email.objects.filter(email=instance.email):
            # reserved for sanity checks soon
            pass
        else:
            mm = Email(user=instance, email=instance.email, active=True)
            mm.save()

post_save.connect(update_emails, sender=User, dispatch_uid="update_emails")

class EmailForm(ModelForm):

    def clean(self):
        cleaned_data = super(EmailForm, self).clean()
        emailadds = Email.objects.filter(email=cleaned_data.get('email'))
        if emailadds:
            raise ValidationError("Duplicate Email")
        return cleaned_data

    class Meta:
        model = Email
        exclude = ['active', 'user']

